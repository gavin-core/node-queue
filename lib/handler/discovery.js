const networking = require('node-network')

const RpcManager = networking.RpcManager

module.exports = function (context) {
  const _this = context || this
  const rpcs = ['GET_SERVICE', 'GET_SERVICE_INTERFACE', 'GET_DB_SERVER', 'GET_ENV_VAR']

  RpcManager.call(_this, 'SPAWN_', [], rpcs)

  _this.getService = (type, version, cb) => {
    _this.rpcs.getService({ type: type, version: version }, cb || (() => {}))
  }

  _this.getServiceInterface = (type, version, interfaceKey, cb) => {
    _this.rpcs.getServiceInterface({ type: type, version: version, interfaceKey: interfaceKey }, cb || (() => {}))
  }

  _this.getDbServer = (name, cb) => {
    _this.rpcs.getDbServer({ name: name }, cb || (() => {}))
  }

  _this.getEnvVar = (key, cb) => {
    _this.rpcs.getEnvVar({ key: key }, cb || (() => {}))
  }

  return _this
}
