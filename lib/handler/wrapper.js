require('./prototypes')

const requireFile = process.argv[2]

process.on('SIGINT', () => {
  console.log('sigint')
})

const networking = require('node-network')
const SocketPacket = require('socket-packet')

const _this = networking.RpcManager.call(this, 'SPAWN_', {}, ['GET_DATA'])

SocketPacket.bind(process.stdin, null, {
  packetParser: packet => packet && JSON.parse(packet)
})
SocketPacket.bind(process.stdout, null, {
  packetStringifier: packet => packet && JSON.stringify(packet)
})

/*************************************************************************/
/** **************************** STDIN ************************************/

process.stdin.on('packet', data => {
  if (data.__rid in _this.rpcCBs) {
    const fn = _this.rpcCBs[data.__rid]
    delete _this.rpcCBs[data.__rid]
    return fn(data.err, data.data)
  }

  const id = data.__id
  const idCamelCased = id.toCamelCase()
  const resp = new function initResponse (id, data) {
    this.send = (d, cb) => {
      _this.sendJson(id, d, cb, data.__rid)
    }

    this.sendError = (err, cb) => {
      _this.sendError(id, err, cb, data.__rid)
    }
  }(id, data)

  if (idCamelCased in _this.hpcs) {
    return _this.hpcs[idCamelCased](data, resp)
  }

  // console.log('unknown-message: ' + id);
  // _this.emit('unknown-message', id, data, resp);
  // logger.error('No packet handler found for id:' + id);

  _this.emit(data.__id, data)
})

/*************************************************************************/
/** *************************** STDOUT ************************************/

_this.sendJson = (id, data, cb, rid) => {
  cb = cb || (() => {});
  process.stdout.dispatch({ __id: id, __rid: rid, data: data }, cb)
}

_this.sendError = (id, err, cb, rid) => {
  cb = cb || (() => {});
  process.stdout.dispatch({ __id: id, __rid: rid, err: err }, cb)
}

/*************************************************************************/

console.log = message => {
  _this.sendJson('console', { message })
}

/*************************************************************************/

const _returnToQueue = (err, result) => {
  let handleType = ''
  if (err) {
    handleType = err.handleType
    delete err.handleType
  }

  const returnObj = {
    success: !err,
    handleType: handleType,
    data: err || result
  }

  _this.sendJson('complete', returnObj, () => {
    process.exit()
  })
}

_this.rpcs.getData((err, data) => {
  if (err) {
    return _returnToQueue(err)
  }

  const Discovery = require('./discovery')
  require(requireFile)(data, _returnToQueue, new Discovery(_this))
})

/*************************************************************************/
