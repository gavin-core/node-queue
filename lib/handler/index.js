const core = require('node-core')
const networking = require('node-network')
const discovery = require('node-discovery')
const events = require('events')
const readline = require('readline')
const Spawner = require('./spawner')
const enums = require('../enums')

const Discovery = discovery.Discovery
const logger = core.logger
const servicePackage = discovery.servicePackage
// const Version = core.Version
const _ = core._

const Handler = function (tasks, handlerCount) {
  /****************************************************************************************************************/
  // SAFE EXITING

  const _onExit = () => {
    logger.verbose(`${servicePackage.name} has been asked to terminate`)

    if (process.askedToTerminate) {
      return
    }

    process.askedToTerminate = true
    _deregister(() => {
      _tryKill()
      setInterval(_tryKill, 1000)
    })
  }

  const _tryKill = () => {
    if (_readyToTerminate()) {
      logger.verbose(`${servicePackage.name} terminating`)
      process.exit()
    }
  }

  process.on('SIGINT', _onExit)
    .on('SIGTERM', _onExit)
    .on('SAFE_KILL', _onExit)

  if (process.platform === 'win32') {
    const rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout
    })

    rl.on('SIGINT', function () {
      process.emit('SIGTERM')
    })
  }

  /****************************************************************************************************************/

  const _this = this
  const _tasks = tasks
  const _handlerCount = handlerCount || 5

  let _client
  let _currentTasks = 0

  _this.connected = false

  const _deregister = cb => {
    cb = cb || (() => {})

    if (!_this.connected) {
      return cb()
    }

    const _onDisconnect = () => {
      cb()
    }

    _client.on('disconnect', _onDisconnect)

    _client.rpcs.deregister(() => {
      logger.debug('deregistered')
      _client.removeListener('disconnect', _onDisconnect)
      cb()
    })
  }

  const _readyToTerminate = () => {
    logger.debug(_currentTasks)
    return _currentTasks <= 0
  }

  _this.tryConnect = () => {
    if (_this.connected || process.askedToTerminate) {
      return
    }

    Discovery.getServiceInterface('queue-service', discovery.servicePackage.services['queue-service'].version, 'tcp', (err, _interface) => {
      if (err || !_interface) {
        return
      }

      const hpcs = {
        handle: (req, res) => {
          const item = req.data

          if (!_.contains(_tasks, `${item.task}@${item.version}`)) {
            logger.warning(`Item rejected: ${req.data.id}`)
            logger.error(`This handler hasn't registered this task, why have you assigned it to me`)
            return res.sendError(`This handler hasn't registered this task, why have you assigned it to me`)
          }

          if (_currentTasks >= _handlerCount) {
            logger.warning(`Item rejected: ${req.data.id}`)
            logger.warning('I am currently full with tasks and cannot handle an over allocation')
            return res.sendError('I am currently full with tasks and cannot handle an over allocation')
          }

          _currentTasks++

          res.send(null, () => {
            const spawner = new Spawner(item.task, item.version, item.data)
            const _onDisconnect = () => {
              spawner.abort()
            }

            _client.on('disconnect', _onDisconnect)

            spawner.on('handled', data => {
              _client.removeListener('disconnect', _onDisconnect)
              _currentTasks--
              if (_this.connected) {
                _client.rpcs.handled({ id: item.id, data: data })
              }
            })

            spawner.on('failed', err => {
              _client.removeListener('disconnect', _onDisconnect)
              _currentTasks--
              if (_this.connected) {
                _client.rpcs.failed({ id: item.id, err: err })
              }
            })

            spawner.on('delete', err => {
              _client.removeListener('disconnect', _onDisconnect)
              _currentTasks--
              if (_this.connected) {
                _client.rpcs.delete({ id: item.id, err: err })
              }
            })
          })
        }
      }

      const rpcs = ['REGISTER', 'DEREGISTER', 'HANDLED', 'FAILED', 'DELETE']

      _client = new networking.SocketClient({ host: _interface.address, port: _interface.port }, hpcs, rpcs)
      _client.on('connect', () => {
        _this.connected = true
        logger.debug('Established connection to queue-service')
        _this.emit('connected')
        _client.rpcs.register({ handlerCount: handlerCount, tasks: _tasks, type: enums.CLIENT_TYPES.QUEUE_ITEM_HANDLER }, () => {
          _this.emit('registered')
        })
      })

      _client.on('error', err => {
        console.log('Error received but not catered for')
        console.error(err)
      })

      _client.on('disconnect', () => {
        _this.connected = false
        _this.emit('disconnect')
        logger.debug('Connection lost to queue-service')
      })
    })
  }

  ;[
    function discover (next, data) {
      Discovery.discover((err, discInfo) => {
        if (err) {
          console.log('Error received but not catered for')
          console.error(err)
        }

        // TODO handle discovery discover error
        logger.debug(`Handler starting on ${new Date()}`)
        data.discInfo = discInfo
        next()
      })
    },
    function registerWithDiscovery (next, data) {
      Discovery.register(next)
    },
    function listeners (next, data) {
      Discovery.on('service-registered', service => {
        if (_this.connected) {
          return
        }

        if (service.name !== 'queue-service' /* || !new Version(service.version).matches(servicePackage.services['queue-service'].version) */) {
          return
        }

        _this.tryConnect()
      })
      next()
    },
    function createHandler (next, data) {
      _this.tryConnect()
      next()
    }
  ].callInSequence(err => {
    if (err) {
      logger.error(err)
      return _onExit()
    }
  })

  return _this
}

Handler.prototype = events.EventEmitter.prototype
module.exports = Handler
