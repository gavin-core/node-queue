const childProcess = require('child_process')
const events = require('events')
const fs = require('fs')
const path = require('path')
const networking = require('node-network')
const discovery = require('node-discovery')
const SocketPacket = require('socket-packet')

const Discovery = discovery.Discovery
const RpcManager = networking.RpcManager
const logger = discovery.logger

const Spawner = function (task, version, data) {
  const _this = this

  logger.verbose(`Handling task: ${task}@${version}`)

  const _getFileName = (basePath, version, allowIndex) => {
    if (version) {
      basePath = path.join(basePath, version.toString())
    }

    const taskName = task.unCamelCase('-', -1)
    const dirContents = fs.readdirSync(basePath)
    let foundDir = false
    let foundFile = false

    for (let i = 0; i < dirContents.length; i++) {
      if (dirContents[i] === taskName) {
        foundDir = true
        break
      } else if (dirContents[i] === taskName + '.js') {
        foundFile = true
        break
      } else if (allowIndex && dirContents[i] === 'index.js') {
        return path.join(basePath, 'index.js')
      }
    }

    if (!foundDir && !foundFile) {
      return
    }

    if (foundFile) {
      return path.join(basePath, taskName + '.js')
    }

    return _getFileName(path.join(basePath, taskName), null, true)
  }

  const taskHandlerFile = _getFileName(path.join(require.main.filename.substring(0, require.main.filename.lastIndexOf(path.sep))), version)
  const spawn = childProcess.spawn('node', [path.join(__dirname, './wrapper.js'), taskHandlerFile])
  SocketPacket.bind(spawn.stdin, null, { 
    packetStringifier: packet => packet && JSON.stringify(packet)
  })
  SocketPacket.bind(spawn.stdout, null, { 
    packetParser: packet => packet && JSON.parse(packet)
  })
  let _handled = false

  const hpcs = {
    getData: (req, res) => {
      res.send(data)
    },
    getService: (req, res) => {
      Discovery.getService(req.data.type, req.data.version, (err, service) => {
        if (err) {
          return res.sendError(err)
        }
        res.send(service)
      })
    },
    getServiceInterface: (req, res) => {
      Discovery.getServiceInterface(req.data.type, req.data.version, req.data.interfaceKey, (err, service) => {
        if (err) {
          return res.sendError(err)
        }
        res.send(service)
      })
    },
    getDbServer: (req, res) => {
      Discovery.getDbServer(req.data.name, (err, dbServer) => {
        if (err) {
          return res.sendError(err)
        }
        res.send(dbServer)
      })
    },
    getEnvVar: (req, res) => {
      Discovery.getEnvVar(req.data.key, (err, varValue) => {
        if (err) {
          return res.sendError(err)
        }
        res.send(varValue)
      })
    }
  }

  const rpcs = []

  RpcManager.call(_this, 'SPAWNER_', hpcs, rpcs)

  const _handleSuccess = data => {
    if (_handled) {
      return
    }

    logger.capture(`Item Success: ${task}@${version}`)
    _this.emit('handled', data)
    _handled = true
  }

  const _handleFailure = data => {
    if (_handled) {
      return
    }

    logger.error(`Item Failed: ${task}@${version}`)
    _this.emit('failed', data)
    _handled = true
  }

  const _handleError = data => {
    if (_handled) {
      return
    }

    logger.error(`Item Errored: ${task}@${version}`)
    _this.emit('failed', data)
    _handled = true
  }

  const _handleDelete = data => {
    logger.warning(`Item identified to delete: ${task}@${version}`)
    _this.emit('delete', data)
    _handled = true
  }

  spawn.stdout.on('packet', data => {
    if (data.__rid in _this.rpcCBs) {
      const fn = _this.rpcCBs[data.__rid]
      delete _this.rpcCBs[data.__rid]
      return fn(data.err, data.data)
    }

    const id = data.__id
    const idCamelCased = id.toCamelCase()
    const resp = new function initResponse (id, data) {
      this.send = (d, cb) => {
        _this.sendJson(id, d, cb, data.__rid)
      }

      this.sendError = (err, cb) => {
        _this.sendError(id, err, cb, data.__rid)
      }
    }(id, data)

    if (idCamelCased in _this.hpcs) {
      return _this.hpcs[idCamelCased](data, resp)
    }

    // console.log('unknown-message: ' + id)
    // _this.emit('unknown-message', id, data, resp)
    // logger.error('No packet handler found for id:' + id)

    _this.emit(data.__id, data.data)
  })

  _this.sendJson = (id, data, cb, rid) => {
    cb = cb || (() => {})


    spawn.stdin.dispatch({ __id: id, __rid: rid, data: data }, cb)
  }

  _this.sendError = (id, err, cb, rid) => {
    cb = cb || (() => {})
    spawn.stdin.dispatch({ __id: id, __rid: rid, err: err }, cb)
  }

  spawn.stderr.on('data', data => {
    data = data.toString()
    console.log(data)
    _handleError(data)
  })

  spawn.on('close', () => {
    if (_handled) {
      return
    }
    _handleError()
  })

  _this.abort = () => {
    spawn.kill('SIGTERM')
  }

  _this.on('console', data => {
    console.log(data.message)
  })

  _this.on('complete', data => {
    if (data.success) {
      return _handleSuccess(data.data)
    }

    data.handleType = data.handleType || ''

    switch (data.handleType.toLowerCase()) {
      case 'delete':
        return _handleDelete(data.data)
      default: // handle general error
        return _handleFailure(data.data)
    }
  })

  return _this
}

Spawner.prototype = events.EventEmitter.prototype
module.exports = Spawner
