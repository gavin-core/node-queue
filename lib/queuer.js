const discovery = require('node-discovery')
const networking = require('node-network')
const Discovery = discovery.Discovery

module.exports = {
  pushItems: function (queueItems, versionOfQueueIntakeService, cb, _Discovery) {
    cb = cb || function () {}

    switch (arguments.length) {
      case 2:
        if (typeof versionOfQueueIntakeService === 'function') {
          cb = versionOfQueueIntakeService
          versionOfQueueIntakeService = discovery.servicePackage.services['queue-intake-service'].version
        }
        break
      case 3:
        if (typeof versionOfQueueIntakeService === 'function') {
          _Discovery = cb
          cb = versionOfQueueIntakeService
          versionOfQueueIntakeService = discovery.servicePackage.services['queue-intake-service'].version
        }
        break
      case 4:
        break
      default:
        return cb(new Error('Invalid number of arguments supplied to Queuer.pushItem; arguments are (queueItems, versionOfQueueIntakeService{optional}, cb)'))
    }

    if (typeof queueItems !== 'object') {
      return cb(new Error('Invalid argument supplied to Queuer.pushItem; arguments are (queueItems, versionOfQueueIntakeService{optional}, cb)'))
    }

    if (!(queueItems instanceof Array)) {
      queueItems = [queueItems]
    }

    ;(_Discovery || Discovery).getServiceInterface('queue-intake-service', versionOfQueueIntakeService, 'web', (err, _interface) => {
      if (err) {
        return cb(err)
      }

      if (!_interface) {
        return cb(new Error('No queue-intake-service/' + versionOfQueueIntakeService + ' available at this time'))
      }

      networking.http.request({
        host: _interface.host,
        port: _interface.port,
        path: '/items',
        method: 'POST'
      }, {
        items: queueItems
      }, cb)
    })
  }
}
