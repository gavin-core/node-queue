const core = require('node-core')
const events = require('events')
const discovery = require('node-discovery')
const networking = require('node-network')
const enums = require('./enums')

const Discovery = discovery.Discovery
const logger = discovery.logger
const _ = core._

const EventListener = function (_events) {
  const _this = this

  ;(() => {
    const _tryConnect = () => {
      if (_this.connected || process.askedToTerminate) {
        return
      }

      Discovery.getServiceInterface('queue-service', discovery.servicePackage.services['queue-service'].version, 'tcp', (err, _interface) => {
        if (err || !_interface) {
          return
        }

        const keys = _.keys(_events)
        const hpcs = {}
        const rpcs = ['REGISTER', 'DEREGISTER', 'CLAIM']

        const _client = new networking.SocketClient({ host: _interface.address, port: _interface.port }, hpcs, rpcs)
        _client.on('connect', () => {
          _this.connected = true
          logger.debug('Established connection to queue-service')
          _this.emit('connected')
          _client.rpcs.register({ type: enums.CLIENT_TYPES.EVENT_LISTENER, events: keys }, err => {
            if (err) {
              console.log('Error received but not catered for')
              console.error(err)
            }

            _this.emit('registered')
          })
        })

        _client.on('error', err => {
          console.log('Error on event-listener but not catered for')
          console.error(err)
        })

        _client.on('disconnect', () => {
          _this.connected = false
          _this.emit('disconnect')
          logger.debug('Connection lost to queue-service')
        })

        for (let index in keys) {
          if (keys.hasOwnProperty(index)) {
            ;(function (key) {
              _client.on(key, data => {
                const req = {
                  __id: key,
                  data: data
                }

                const res = {
                  claim: cb => {
                    _client.rpcs.claim(data, cb)
                  }
                }

                _events[key](req.data, res)
              })
            })(keys[index])
          }
        }
      })
    }

    setInterval(_tryConnect, 100)
  })()

  return _this
}

EventListener.prototype = events.EventEmitter.prototype
module.exports = EventListener
